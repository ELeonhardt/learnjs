const tabs = document.querySelector('.tabs');
const tabButtons = tabs.querySelectorAll('[role="tab"]');
const tabPanels = Array.from(tabs.querySelectorAll('[role="tabpanel"]'));

function handleTabEvent(event) {
  // hide all tab panels
  tabPanels.forEach((tabPanel) => {
    tabPanel.hidden = true;
  });

  // mark all tabs as unselected
  tabButtons.forEach((tabButton) => {
    tabButton.setAttribute('aria-selected', false);
  });
  // mark the clicked tab as selected
  event.currentTarget.setAttribute('aria-selected', true);
  // find the associated tabpanel and show it
  // first: get the id of the button
  const buttonId = event.currentTarget.id;
  // second: find the tabPanel with the matching aria-labelledby
  const showingTabPanel = tabPanels.find(
    (element) => element.getAttribute('aria-labelledby') === `${buttonId}`
  );
  console.log(showingTabPanel);
  showingTabPanel.hidden = false;
}
// add eventlistener to listen for clicks on tabButtons
tabButtons.forEach((button) => {
  button.addEventListener('click', handleTabEvent);
});

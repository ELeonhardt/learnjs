const shoppingForm = document.querySelector('.shopping');
const list = document.querySelector('.list');

let items = [];
function displayItems() {
  // the aria label is for screenreaders
  // only puts checked if the item.complete is true
  const html = items
    .map(
      (item) => `<li class="shopping-item">
  <input value="${item.id}" ${item.complete && 'checked'} type="checkbox">
  <span class="itemName">${item.name}</span>
  <button value="${item.id}" aria-label="Remove ${item.name}">&times;</button>
  </li>`
    )
    .join('');
  list.innerHTML = html;
}

function handleSubmit(e) {
  e.preventDefault(); // needs to be prevented because submit is a default action
  // get the inserted text out of the input field
  const insertedText = e.currentTarget.item.value; // item is accessible because the name of the input
  // create a new item for the state
  const item = {
    name: insertedText,
    id: Date.now(),
    complete: false,
  };
  // add the new item to the state
  items.push(item);

  // clear the form (works also with several inputs)
  // In the case of a form event, it actually triggers on the form and doesn't bubble
  // therefore, e.currentTarget and e.target are the same here:
  e.currentTarget.reset();
  // displayItems(); instead of this, we are going to fire off a custom event that tells everybody who needs to know about it that the list was updated
  list.dispatchEvent(new CustomEvent('itemsUpdated')); // firing off a custom event
}

function mirrorToLocalStorage() {
  console.info('saving items to localstorage');
  localStorage.setItem('items', JSON.stringify(items));
}

function restoreFromLocalStorage() {
  console.log('restoring from Local storage');
  const ls = JSON.parse(localStorage.getItem('items'));
  console.log(ls);
  if (ls.length) {
    items.push(...ls); // spread the restored item into the items object
  }
  list.dispatchEvent(new CustomEvent('itemsUpdated'));
}

function buying(id) {
  console.log('item has been bought', id);
  // find and update the item
  const boughtItem = items.find((item) => item.id === parseInt(id));
  boughtItem.complete = !boughtItem.complete;
  console.log(items);
  list.dispatchEvent(new CustomEvent('itemsUpdated'));
}

function deleteItem(id) {
  const reducedItems = items.filter((item) => item.id !== parseInt(id));
  items = reducedItems;
  list.dispatchEvent(new CustomEvent('itemsUpdated'));
}

shoppingForm.addEventListener('submit', handleSubmit);
// attaching eventlistener to my custom event
// this decouples
list.addEventListener('itemsUpdated', displayItems);
list.addEventListener('itemsUpdated', mirrorToLocalStorage);
list.addEventListener('click', (e) => {
  if (e.target.matches('button')) {
    // target has a class of button
    deleteItem(e.target.value);
  }
  if (e.target.matches('input[type="checkbox"]')) {
    buying(e.target.value);
  }
});
restoreFromLocalStorage();

// Select the elements on the page (canvas and shake button)
const canvas = document.querySelector('#etch-a-sketch');
// context = the thing onto which the drawing will be rendered.
const ctx = canvas.getContext('2d');
const shakebutton = document.querySelector('.shake');
const MOVE_AMOUNT = 10;
let hue = 0;
// Setup our canvas for drawing

ctx.lineJoin = 'round'; // corners of joined lines are rounded
ctx.lineCap = 'round'; // ends of lines
ctx.lineWidth = 10;

// destructuring: taking properties of an object an assigning them to equally named variables
// create random x and y starting point
const { width, height } = canvas;
let x = Math.floor(Math.random() * width);
let y = Math.floor(Math.random() * height);

// this is how you draw something (a dot, in this case)
ctx.beginPath();
ctx.moveTo(x, y);
ctx.lineTo(x, y);
ctx.strokeStyle = `hsl(${hue}, 100%, 50%)`;

// write the draw function
// I can also use destructuring in function declarations (but I need to pass in all of the arguments?!)
function draw({ key }) {
  // increment the hue
  hue += 5;
  ctx.strokeStyle = `hsl(${hue}, 100%, 50%)`;
  ctx.beginPath();
  ctx.moveTo(x, y);
  // moving based on user input
  switch (key) {
    case 'ArrowUp':
      y -= MOVE_AMOUNT;
      break;
    case 'ArrowDown':
      y += MOVE_AMOUNT;
      break;
    case 'ArrowRight':
      x += MOVE_AMOUNT;
      break;
    case 'ArrowLeft':
      x -= MOVE_AMOUNT;
      break;
    default:
      break;
  }
  ctx.lineTo(x, y);
  ctx.stroke();
}

// write a handler for the keys
function handleKey(e) {
  if (e.key.includes('Arrow')) {
    e.preventDefault();
    draw({ key: e.key });
  }
}

// clear / shake function
function clearCanvas() {
  canvas.classList.add('shake');
  ctx.clearRect(0, 0, width, height);
  canvas.addEventListener(
    'animationend',
    function () {
      canvas.classList.remove('shake');
      console.log('did the shake');
    },
    { once: true }
  );
}
// listen for arrow keys
window.addEventListener('keydown', handleKey);
shakebutton.addEventListener('click', clearCanvas);

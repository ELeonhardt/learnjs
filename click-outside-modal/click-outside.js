const buttons = document.querySelectorAll('button');
const modalOuter = document.querySelector('.modal-outer');
const modalInner = document.querySelector('.modal-inner');

function handleButton(e) {
  modalOuter.classList.add('open');
  // to get the image source, I need to climb up in the html
  const containinigCard = e.currentTarget.closest('.card');
  // get all the needed info out of the containingCard object
  const imgSrc = containinigCard.querySelector('img').src;
  const { description } = containinigCard.dataset;
  modalInner.innerHTML = `
    <img width=600 height=600 src="${imgSrc.replace('200', '600')}" alt="">
      <p>${description}</p>
  `;
}

function handleModalClick(e) {
  console.log(e);
  const outsideModal = !e.target.closest('.modal-inner');
  if (outsideModal) {
    modalOuter.classList.remove('open');
  }
}
buttons.forEach(function (button) {
  button.addEventListener('click', handleButton);
});

modalOuter.addEventListener('click', handleModalClick);
window.addEventListener('keyup', (event) => {
  if (event.key === 'Escape') {
    modalOuter.classList.remove('open');
  }
});
